
/* eslint-env jquery */

const table = $("#pixel_canvas");
let color = $("#colorPicker").val();
$("#colorPicker").change(function(){
    return color = $(this).val();
});
// Take and validate input values, create table if validation passes
function makeGrid() {
    let height = $("#input_height").val();
    let width = $("#input_width").val();

    table.children().remove();

    for (let h = 0; h < height; h++)
        if (height > 50)
            return alert("Maximum allowed canvas height is 50");
        else
            table.append("<tr></tr>");
    for (let w = 0; w < width; w++)
        if (width > 50)
            return alert("Maximum allowed canvas width is 50");
        else
            table.children().append("<td></td>");
}
// Make whole table white
function clearGrid() {
    table.find("td").css("background-color", "#fff");
}
// Draw & Erase events
table.on("click", "td", function() {
    $(this).css("background-color", color);
});

table.on("contextmenu", "td", function(event) {
    event.preventDefault();
    $(this).css("background-color", "#fff");
});

let holdDownLeft = false;
let holdDownRight = false;
table.on("mousedown", "td", function(event){
    if (event.which == 1)
        holdDownLeft = true,
        $(this).css("background-color", color);
    else if (event.which == 3)
        holdDownRight = true,
        $(this).css("background-color", "#fff");
});

table.on("mouseup", "td", function(){
    holdDownLeft = false;
    holdDownRight = false;
});

table.on("mouseover", "td", function(){
    if(holdDownLeft == true)
        $(this).css("background-color", color);
    else if (holdDownRight == true)
        $(this).css("background-color", "#fff");
});
// Execute on button clicks
$("#create").click(function(e) {
    e.preventDefault(); // Prevents page reloading
    $("small").html("Left click: Color - Right click: Erase");
    makeGrid();
});

$("#clear").click(function(e) {
    e.preventDefault();
    clearGrid();
});
